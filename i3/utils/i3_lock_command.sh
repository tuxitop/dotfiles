# i3_lock_command.sh
# locks the screen using i3lock-color
#
# Author: Ali Mousavi <ali.mousavi@gmail.com>
# Last Updated: 2020/08/26
# 
# Dependencies:
#   - i3lock-color

# pkill -u $USER -USR1 dunst 2>&1 >/dev/null
dunstctl set-paused true
i3lock --nofork \
       --clock \
       --blur 8 \
       --keylayout 2 \
       --line-uses-inside \
       --keyhlcolor=d23c3dff \
       --bshlcolor=d23c3dff \
       --separatorcolor=00000000 \
       --insidevercolor=fecf4dff \
       --insidewrongcolor=d23c3dff \
       --ringvercolor=ffffffff \
       --ringwrongcolor=ffffffff \
       --timecolor=ffffffff \
       --datecolor=ffffffff
# pkill -u $USER -USR2 dunst 2>&1 >/dev/null
dunstctl set-paused false
