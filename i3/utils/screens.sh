#! /bin/bash

# Monitor hotplug script (with the help of udev.)
# 
# Author: Ali Mousavi <ali.mousavi@gmail.com>
# Last Updated: 2018/07/24
# 
# - Dependencies:
#   xrandr
#   wireless_tools (Optional - Attach based on the ESSID of the wifi)
#
#
# For the hotplug functionality udev rules should be configured to
# run the script in the event of attaching or dettaching a display.
# e.g:
#
# cat /etc/udev/rules.d/95-monitor-hotplug.rules
# 
# KERNEL=="card0", SUBSYSTEM=="drm", ENV{DISPLAY}=":0", ENV{XAUTHORITY}="/home/ali/.Xauthority", RUN+="/path/to/screen.sh true"
#


HOME_NET="alifa"  # SSID of the home network.
HOME_EXT_RES="1920x1080" # Resolution of the external monitor at home.
HOME_PRI_RES="1920x1080" # Resolution of the external monitor at home.
HOME_EXT_POS="0x0"    # Position of the external monitor at home.
HOME_PRI_POS="0x1080"    # Position of the primary monitor at home.
EXT_MONITOR="HDMI-1-0"     # Name of the exteral monitor.
PRI_MONITOR="eDP2"      # Name of the primary monitor.

SSID=$(iwgetid -r 2>/dev/null)
IS_MON_CONNECTED=$(xrandr | grep "${EXT_MONITOR} connected" > /dev/null 2>&1 && echo "true")

sleep 1

if [[ $SSID = $HOME_NET && $IS_MON_CONNECTED = "true" ]]; then
  xrandr --output ${EXT_MONITOR} --mode ${HOME_EXT_RES} --pos ${HOME_EXT_POS} --rotate normal --output ${PRI_MONITOR} --primary --mode ${HOME_PRI_RES} --pos ${HOME_PRI_POS} --rotate normal
elif [[ $IS_MON_CONNECTED = "true" ]]; then
  xrandr --output ${PRI_MONITOR} --auto --output ${EXT_MONITOR} --auto
else
  xrandr --output ${PRI_MONITOR} --auto --output ${EXT_MONITOR} --off 
fi

# Restart i3 if required
if [[ $1 == "true" ]]; then
  i3-msg -t command restart
fi

exit 0

