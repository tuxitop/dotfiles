#!/bin/bash

# i3_startup.sh
# start some applications only if connected to home network.

# Author: Ali Mousavi <ali.mousavi@gmail.com>
# Last Updated: 2018/07/24
#
# Dependencies:
#   - wireless_tools
#   - tmux
#   - firefox-developer-edition
#   - hexchat
#   - telegram-desktop


HOME_NET="theTerminal_nomap"

PS=$(ps aux)

# Start tmux if a tmux session (named i3)
if ! (tmux list-sessions 2>/dev/null | grep -q "^i3"); then
    termite -e "bash -c 'tmux new-session -s i3 -d; tmux split-window -h; tmux -2 attach-session -d'" 2>/dev/null 1>&2 &
else
    termite -e "bash -c 'tmux attach'" >/dev/null 2>&1 &
fi

# Start firefox if connected to Home Wireless.
SSID=$(iwgetid -r)
if [[ $SSID = $HOME_NET ]]; then
    if ! (echo $PS | grep -q firefox-developer ); then
        firefox-developer-edition 2>/dev/null 1>&2 &
    fi;
    # if ! (echo $PS | grep -q hexchat); then
    #     hexchat 2>/dev/null 1>&2 &
    # fi
    if ! (echo $PS | grep -q telegram-deskto); then
        telegram-desktop 2>/dev/null 1>&2 &
    fi
fi;
