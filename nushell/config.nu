# config.nu
#
# Installed by:
# version = "0.101.0"
#
# This file is used to override default Nushell settings, define
# (or import) custom commands, or run any other startup tasks.
# See https://www.nushell.sh/book/configuration.html
#
# This file is loaded after env.nu and before login.nu
#
# You can open this file in your default editor using:
# config nu
#
# See `help config nu` for more options
#
# You can remove these comments if you want or leave
# them for future reference.


### Nu Configurations
$env.config.buffer_editor = "vim"

# Use carapace as completer
# https://carapace-sh.github.io/
# https://carapace-sh.github.io/carapace-bin/setup/environment.html#carapace_bridges
$env.CARAPACE_BRIDGES = 'zsh,fish,bash,inshellisense'
source ~/.cache/carapace/init.nu

# Use Starship as the prompt
# https://starship.rs/#nushell
# https://www.nushell.sh/book/3rdpartyprompts.html#starship
mkdir ($nu.data-dir | path join "vendor/autoload")
starship init nu | save -f ($nu.data-dir | path join "vendor/autoload/starship.nu")

### Environment Variables
$env.EDITOR = "vim"

### Aliases
alias pacman = sudo pacman
