# ~/.tmux.conf
#
# Author: Ali Mousavi
# Last Updated: 2014/08/21
#
# Dependencies:
# xsel (copy & paste via clipboard)

########################################
# KEY BINDING:
########################################

# change prefix key
set -g prefix C-a
unbind C-b
bind C-a send-prefix

# map Vi movement keys as pane movement keys
bind h select-pane -L
bind j select-pane -D
bind k select-pane -U
bind l select-pane -R

# Better Splitting
bind | split-window -h
bind _ split-window -v


########################################
# TWEAKS FOR BETTER USABILITY
########################################

# Use mouse wheel to enter/exit copy mode (to scroll the buffer)
# setw -g mode-mouse on

# Enable 256 colors.
set -g default-terminal "screen-256color"

##CLIPBOARD selection integration
##Requires prefix key before the command key
#Copy tmux paste buffer to CLIPBOARD
#bind C-c run "tmux save-buffer - | xclip -i"
bind y run-shell "tmux show-buffer | xclip -sel clip -i" \; display-message "Copied tmux buffer to system clipboard"
#Copy CLIPBOARD to tmux paste buffer and paste tmux paste buffer
bind C-v run "tmux set-buffer -- \"$(xsel -o -b)\"; tmux paste-buffer"

# Set window notifications
setw -g monitor-activity on
set -g visual-activity on

# Window Titles
set -g set-titles on
set -g set-titles-string "#T"

# Default shell:
#set-option -g default-shell /usr/bin/fish

########################################
# STATUS BAR
########################################

set -g status-bg black
set -g status-fg white

# Left side of status bar
set -g status-left-length 20
set -g status-left '#[fg=green][#[bg=black,fg=cyan]#S#[bg=black,fg=blue,dim]:#T#[fg=green]]'

# Alerted window in status bar. Windows which have an alert (bell, activity or
# content).
#set-window-option -g window-status-alert-fg red
#set-window-option -g window-status-alert-bg white

# Right side of status bar
#set -g status-right '#[fg=yellow]#(date)
set -g status-right '#[fg=green][#[fg=white]#H#[fg=green]][#[fg=blue]%Y-%m-%d #[fg=white]%H:%M#[default]#[fg=green]]'

