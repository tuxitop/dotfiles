#!/usr/bin/bash

# file:         andropush
# description:  Send messages to another device using pushbullet api
#
# usage:        andropush "message body" ["message title"]
#
# requirements: Install PushBullet app on your device and then get
#               your Access-Token from its website and paste below.
# author:       Ali Mousavi (ali.mousavi@gmail.com)
# Created On:   2017-06-13
# Last Updated: 2018-07-25


ACCESS_TOKEN=""   # <= Paste your access token here

SCRIPT=$(realpath $0)
SCRPATH=$(dirname $SCRIPT)

if [ -n "$ACCESS_TOKEN" ]; then
  PUSHBULLET_ACCESS_TOKEN="$ACCESS_TOKEN"
fi

# Try to find the access token
if [ -z $PUSHBULLET_ACCESS_TOKEN ];then
  if [ -f ${SCRPATH}/.andropush_env ]; then
    source ${SCRPATH}/.andropush_env
  fi
fi

TOKEN=$PUSHBULLET_ACCESS_TOKEN
if [ -z $TOKEN ]; then
  echo "You should provide an Access-Token either by exporting \$PUSHBULLET_ACCESS_TOKEN \
        or by setting it in the script"
  exit 1
fi

MSG="$1"
if [ -z "$2" ]
then
  TITLE="Notification From Linux"
else
  TITLE="$2"
fi

curl --header "Access-Token: ${TOKEN}" \
     --header "Content-Type: application/json" \
     --data-binary "{\"body\":\"${MSG}\",\"title\":\"${TITLE}\",\"type\":\"note\"}" \
     --request POST \
     https://api.pushbullet.com/v2/pushes
