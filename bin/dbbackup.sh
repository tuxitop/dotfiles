#! /bin/bash

# dbbackup.sh
# Backup databases

# Author: Ali Mousavi
# Last Updated: 2018/11/09

#--------------------
# Configuration:
#--------------------

# Global Configurations
BACKUP_DIR="/home/ali/Documents/Backup/Databases"
LOG_FILE="/home/ali/Documents/Backup/backup.log"
TEMP_DIR="/var/tmp/"
DB_ACCOUNT="root"           # Database user account
DB_PASS="${DB_PASS}"        # Database password
STOP_MYSQL=0                # Stop mysql service after backup
USER=ali                    # change ownership of backup
GROUP=ali                    # change ownership of backup

# Function to backup databases
db_backup() {

  mysqladmin -u ${DB_ACCOUNT} -p${DB_PASS} status >/dev/null 2>&1
  if (( $?  )); then
     echo "Can't connect to the database"
     echo "Databases: Can't connect to the database" >> ${LOG_FILE}
     return 1
   fi

  local TMP="${TEMP_DIR}/DB_BACKUP_TMP"
  mkdir -p ${TMP}

  # backup all databases except internal databases.
  mysql -u ${DB_ACCOUNT} -p${DB_PASS} -N -e 'show databases' | while read dbname; do
      if [ "$dbname" != "information_schema" ] && [ "$dbname" != "performance_schema" ] && [ "$dbname" != "mysql" ] 
      then
        mysqldump -u "${DB_ACCOUNT}" -p"${DB_PASS}" --complete-insert "${dbname}" > "${TMP}/${dbname}.sql"
      fi
    done

  /usr/bin/rsync -a --quiet ${TMP}/* ${BACKUP_DIR}
  echo "Finished backing up databases"
  echo "Databases: $(date "+%F-%T")" >> ${LOG_FILE}

  # Clean Up
  chown -R ${USER}:${GROUP} ${BACKUP_DIR}
  rm -rf $TMP
}

# Check for db user and password
if [ -z "${DB_ACCOUNT}" ] || [ -z "${DB_PASS}" ]; then
  echo "You have to specify the database username and password in the settings."
  echo "Databases: $(date "+%F-%T") - No username and password specified." >> ${LOG_FILE}
  exit 1
fi

# Try to start mariadb if not already started
if [ "$(systemctl is-active mariadb.service)" = "inactive" ]; then
  if (( $UID )); then
    echo "Databases: $(date "+%F-%T") - Failed to start mariadb service" >> ${LOG_FILE}
    exit 1
  fi

  systemctl start mariadb.service
  if (( $? )); then
    echo "Databases: $(date "+%F-%T") - Failed to start mariadb service" >> ${LOG_FILE}
    exit 1
  fi
  STOP_MYSQL=1
fi

# Perform the backup
db_backup

# Stop mariadb.service if required
if (( ${STOP_MYSQL}  )); then
  systemctl stop mariadb.service
fi
